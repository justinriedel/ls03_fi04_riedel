package model.persistanceDB;

public class AccessDB {

	// Attribute
	private String dbName;
	private String user;
	private String password;
	private String url;

	// Konstruktor
	public AccessDB() {
		this.url = "jdbc:mysql://192.168.8.103/";
		this.dbName = "oszimt_lf05";
		this.user = "aliendefence";
		this.password = "aliendefence#";
	}

	// Getter und Setter
	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getFullURL() {
		return this.url + this.dbName;
	}
}
