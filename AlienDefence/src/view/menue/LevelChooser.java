package view.menue;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.List;

import javax.swing.BorderFactory;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;

import controller.AlienDefenceController;
import controller.GameController;
import controller.LevelController;
import model.Level;
import model.User;
import view.game.GameGUI;

@SuppressWarnings("serial")
public class LevelChooser extends JPanel {

	private LevelController lvlControl;
	private LeveldesignWindow leveldesignWindow;
	private JTable tblLevels;
	private DefaultTableModel jTableData;
	private List<Level> arrLevel;
	
	/**
	 * Create the panel.
	 * 
	 * @param leveldesignWindow
	 * @param alienDefenceController 
	 */
	public LevelChooser(LevelController lvlControl, LeveldesignWindow leveldesignWindow, AlienDefenceController alienDefenceController, String context) {
		this.lvlControl = lvlControl;
		this.leveldesignWindow = leveldesignWindow;
		arrLevel = this.lvlControl.readAllLevels();


		setLayout(new BorderLayout());
		setBackground(Color.BLACK);
		
		
		JPanel pnlButtons = new JPanel();
		pnlButtons.setBackground(Color.BLACK);
		
		add(pnlButtons, BorderLayout.SOUTH);

		if(context == "editing" ) {
		JButton btnNewLevel = new JButton("Neues Level");
		btnNewLevel.setBackground(Color.BLACK);
		btnNewLevel.setFocusPainted(false);
		btnNewLevel.setForeground(Color.WHITE);
	
		btnNewLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				btnNewLevel_Clicked();
			}
		});
		pnlButtons.add(btnNewLevel);
		

		JButton btnUpdateLevel = new JButton("ausgew\u00E4hltes Level bearbeiten");
		btnUpdateLevel.setBackground(Color.BLACK);
		btnUpdateLevel.setFocusPainted(false);
		btnUpdateLevel.setForeground(Color.WHITE);
	
		btnUpdateLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnUpdateLevel_Clicked();
			}
		});
		pnlButtons.add(btnUpdateLevel);

		JButton btnDeleteLevel = new JButton("Level l\u00F6schen");
		btnDeleteLevel.setBackground(Color.BLACK);
		btnDeleteLevel.setFocusPainted(false);
		btnDeleteLevel.setForeground(Color.WHITE);
	
		btnDeleteLevel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				btnDeleteLevel_Clicked();
			}
		});
		pnlButtons.add(btnDeleteLevel);

		}
		

		
		
		if(context=="gameplay") {
			
		JButton btnSpielen = new JButton("Spielen");
		btnSpielen.setBorderPainted(false);
		btnSpielen.setBackground(Color.BLACK);
		btnSpielen.setFocusPainted(false);
		btnSpielen.setForeground(Color.WHITE);
		btnSpielen.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				// Erstellt Modell von aktuellen Nutzer
				User user = new User(1, "test", "pass");

				Thread t = new Thread("GameThread") {

					@Override
					public void run() {
						GameController gameController = alienDefenceController.startGame(arrLevel.get(0), user);
						new GameGUI(gameController).start();
					}
				};
				t.start();
			}
		});
		pnlButtons.add(btnSpielen);
		}
		
		
		JLabel lblLevelauswahl = new JLabel("Levelauswahl");
		lblLevelauswahl.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.CENTER);
		lblLevelauswahl.setForeground(Color.WHITE);
		add(lblLevelauswahl, BorderLayout.NORTH);

		JScrollPane spnLevels = new JScrollPane();
	
		add(spnLevels, BorderLayout.CENTER);

		tblLevels = new JTable();
		tblLevels.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		tblLevels.setFont(new Font("Segoe UI", Font.PLAIN, 15));
		tblLevels.setBackground(Color.BLACK);
		tblLevels.setForeground(Color.WHITE);
		tblLevels.setRowHeight(30);
		tblLevels.setGridColor(Color.BLACK);
		tblLevels.setSelectionBackground(Color.WHITE);
		tblLevels.setSelectionForeground(Color.BLACK);
		tblLevels.setShowGrid(false);
		tblLevels.setIntercellSpacing(new Dimension(0, 0));
		

	      JTableHeader header = tblLevels.getTableHeader();
	      header.setBackground(Color.BLACK);
	      header.setFont(new Font("Segoe UI", Font.BOLD, 15));
	      header.setForeground(Color.WHITE);
			 
		spnLevels.setViewportView(tblLevels);
		spnLevels.getViewport().setBackground(Color.BLACK);
		this.updateTableData();
	}

	private String[][] getLevelsAsTableModel() {
		List<Level> levels = this.lvlControl.readAllLevels();
		String[][] result = new String[levels.size()][];
		int i = 0;
		for (Level l : levels) {
			result[i++] = l.getData();
		}
		return result;
	}

	public void updateTableData() {
		this.jTableData = new DefaultTableModel(this.getLevelsAsTableModel(), Level.getLevelDescriptions());
		this.tblLevels.setModel(jTableData);
	}

	public void btnNewLevel_Clicked() {
		this.leveldesignWindow.startLevelEditor();
	}

	public void btnUpdateLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.leveldesignWindow.startLevelEditor(level_id);
	}

	public void btnDeleteLevel_Clicked() {
		int level_id = Integer
				.parseInt((String) this.tblLevels.getModel().getValueAt(this.tblLevels.getSelectedRow(), 0));
		this.lvlControl.deleteLevel(level_id);
		this.updateTableData();
	}
}
