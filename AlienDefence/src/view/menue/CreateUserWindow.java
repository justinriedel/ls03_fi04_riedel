package view.menue;

import java.awt.BorderLayout;
import java.awt.CardLayout;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import controller.UserController;
import model.User;

import java.awt.Color;
import java.awt.Font;
import java.awt.GridLayout;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import java.awt.FlowLayout;
import java.awt.event.ActionListener;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.time.format.DateTimeParseException;
import java.awt.event.ActionEvent;



@SuppressWarnings("serial")
public class CreateUserWindow extends JFrame {
	
	private UserController userController;
	
	
	private JTextField surNameInput;
	private JTextField firstNameInput;
	private JTextField birthdayInput;
	private JTextField streetInput;
	private JTextField houseNumberInput;
	private JTextField postalCodeInput;
	private JTextField cityInput;
	private JTextField loginNameInput;
	private JTextField passwordInput;
	private JTextField salaryInput;
	private JTextField maritalStatusInput;
	private JTextField finalGradeInput;

	/**
	 * Create the this.
	 * @param alienDefenceController 
	 */
	public CreateUserWindow(UserController userController) {

		
		this.userController = userController;
		
		setTitle("Benutzer erstellen");
		setDefaultCloseOperation(this.DISPOSE_ON_CLOSE);
		setBounds(100, 100, 546, 577);
		setBackground(Color.BLACK);
		
		JLabel lblLevelauswahl = new JLabel("Benutzer erstellen");
		lblLevelauswahl.setOpaque(true);
		lblLevelauswahl.setBackground(Color.BLACK);
		lblLevelauswahl.setFont(new Font("Segoe UI", Font.BOLD, 18));
		lblLevelauswahl.setHorizontalAlignment(SwingConstants.LEFT);
		lblLevelauswahl.setForeground(Color.WHITE);
		getContentPane().add(lblLevelauswahl, BorderLayout.NORTH);
		
			JButton createButton = new JButton("Erstellen");
			createButton.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent arg0) {
					createUser();
				}
			});
				createButton.setForeground(Color.WHITE);
				createButton.setBackground(Color.BLACK);
				createButton.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		getContentPane().add(createButton, BorderLayout.SOUTH);
		

		JPanel formArea = new JPanel();
		formArea.setBackground(Color.BLACK);
		formArea.setForeground(Color.WHITE);
		getContentPane().add(formArea, BorderLayout.CENTER);
		
	      GridLayout formGridLayout = new GridLayout(0,2);
	      formGridLayout.setVgap(10);
	      formGridLayout.setHgap(30);
		formArea.setLayout(formGridLayout);
		
		JLabel surNameLabel = new JLabel("Nachname");
		surNameLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		surNameLabel.setForeground(Color.WHITE);
		formArea.add(surNameLabel);
		
		surNameInput = new JTextField();
		surNameInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		surNameInput.setBackground(Color.BLACK);
		surNameInput.setForeground(Color.WHITE);
		formArea.add(surNameInput);
		surNameInput.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Vorname");
		lblNewLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		lblNewLabel.setForeground(Color.WHITE);
		formArea.add(lblNewLabel);
		
		firstNameInput = new JTextField();
		firstNameInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		firstNameInput.setBackground(Color.BLACK);
		firstNameInput.setForeground(Color.WHITE);
		formArea.add(firstNameInput);
		firstNameInput.setColumns(10);
		
		JLabel birthdayLabel = new JLabel("Geburtstag");
		birthdayLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		birthdayLabel.setForeground(Color.WHITE);
		formArea.add(birthdayLabel);
		
		birthdayInput = new JTextField();
		birthdayInput.setText("01.01.1990");
		birthdayInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		birthdayInput.setBackground(Color.BLACK);
		birthdayInput.setForeground(Color.WHITE);
		formArea.add(birthdayInput);
		birthdayInput.setColumns(10);
		
		JLabel streetLabel = new JLabel("Stra\u00DFe");
		streetLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		streetLabel.setForeground(Color.WHITE);
		formArea.add(streetLabel);
		
		streetInput = new JTextField();
		streetInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		streetInput.setBackground(Color.BLACK);
		streetInput.setForeground(Color.WHITE);
		formArea.add(streetInput);
		streetInput.setColumns(10);
		
		JLabel houseNumberLabel = new JLabel("Hausnummer");
		houseNumberLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		houseNumberLabel.setForeground(Color.WHITE);
		formArea.add(houseNumberLabel);
		
		houseNumberInput = new JTextField();
		houseNumberInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		houseNumberInput.setBackground(Color.BLACK);
		houseNumberInput.setForeground(Color.WHITE);
		formArea.add(houseNumberInput);
		houseNumberInput.setColumns(10);
		
		JLabel postalCodeLabel = new JLabel("PLZ");
		postalCodeLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		postalCodeLabel.setForeground(Color.WHITE);
		formArea.add(postalCodeLabel);
		
		postalCodeInput = new JTextField();
		postalCodeInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		postalCodeInput.setBackground(Color.BLACK);
		postalCodeInput.setForeground(Color.WHITE);
		formArea.add(postalCodeInput);
		postalCodeInput.setColumns(10);
		
		JLabel cityLabel = new JLabel("Stadt");
		cityLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		cityLabel.setForeground(Color.WHITE);
		formArea.add(cityLabel);
		
		cityInput = new JTextField();
		cityInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		cityInput.setBackground(Color.BLACK);
		cityInput.setForeground(Color.WHITE);
		formArea.add(cityInput);
		cityInput.setColumns(10);
		
		JLabel loginNameLabel = new JLabel("Benutzername");
		loginNameLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		loginNameLabel.setForeground(Color.WHITE);
		formArea.add(loginNameLabel);
		
		loginNameInput = new JTextField();
		loginNameInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		loginNameInput.setBackground(Color.BLACK);
		loginNameInput.setForeground(Color.WHITE);
		formArea.add(loginNameInput);
		loginNameInput.setColumns(10);
		
		JLabel passwordLabel = new JLabel("Passwort");
		passwordLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		passwordLabel.setForeground(Color.WHITE);
		formArea.add(passwordLabel);
		
		passwordInput = new JTextField();
		passwordInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		passwordInput.setBackground(Color.BLACK);
		passwordInput.setForeground(Color.WHITE);
		formArea.add(passwordInput);
		passwordInput.setColumns(10);
		
		JLabel salaryLabel = new JLabel("Gehaltsvorstellungen");
		salaryLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		salaryLabel.setForeground(Color.WHITE);
		formArea.add(salaryLabel);
		
		salaryInput = new JTextField();
		salaryInput.setText("0");
		salaryInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		salaryInput.setBackground(Color.BLACK);
		salaryInput.setForeground(Color.WHITE);
		formArea.add(salaryInput);
		salaryInput.setColumns(10);
		
		JLabel maritalStatusLabel = new JLabel("Ehestatus");
		maritalStatusLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		maritalStatusLabel.setForeground(Color.WHITE);
		formArea.add(maritalStatusLabel);
		
		maritalStatusInput = new JTextField();
		maritalStatusInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		maritalStatusInput.setBackground(Color.BLACK);
		maritalStatusInput.setForeground(Color.WHITE);
		formArea.add(maritalStatusInput);
		maritalStatusInput.setColumns(10);
		
		JLabel finalGradeLabel = new JLabel("Abschlussnote");
		finalGradeLabel.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		finalGradeLabel.setForeground(Color.WHITE);
		formArea.add(finalGradeLabel);
		
		finalGradeInput = new JTextField();
		finalGradeInput.setText("1.0");
		finalGradeInput.setFont(new Font("Segoe UI", Font.PLAIN, 12));
		finalGradeInput.setBackground(Color.BLACK);
		finalGradeInput.setForeground(Color.WHITE);
		formArea.add(finalGradeInput);
		finalGradeInput.setColumns(10);

		
		this.setVisible(true);
		

	}

	public void createUser() {
		
		boolean error = false;
		
		
		User user = new User();
		
		if(surNameInput.getText() != null) {
			user.setSur_name(surNameInput.getText());
		} else {
			error = true;
		}
		
		if(firstNameInput.getText() != null) {
			user.setFirst_name(firstNameInput.getText());
		} else {
			error = true;
		}
		
		
		// Birthday Formatting
		DateTimeFormatter f = new DateTimeFormatterBuilder().parseCaseInsensitive()
		        .append(DateTimeFormatter.ofPattern("dd.MM.yyyy")).toFormatter();
		try {
		    LocalDate datetime = LocalDate.parse(birthdayInput.getText(), f);
		    user.setBirthday(datetime);
		} catch (DateTimeParseException e) {
		     error = true;
		     JOptionPane.showMessageDialog(null, "Das Geburtsdatum muss dem deutschen Datumsformat folgen.", "Fehler",
						JOptionPane.ERROR_MESSAGE);
		}
		
		
		
		if(streetInput.getText().length() > 0) {
			user.setStreet(streetInput.getText());
		} else {
			error = true;
		}
		
		if(houseNumberInput.getText().length() > 0) {
			user.setHouse_number(houseNumberInput.getText());
		} else {
			error = true;
		}
		

		if(postalCodeInput.getText().length() > 0) {
			user.setPostal_code(postalCodeInput.getText());
		} else {
			error = true;
		}
		
		if(cityInput.getText().length() > 0) {
			user.setCity(cityInput.getText());
		} else {
			error = true;
		}
		
		
		if(loginNameInput.getText().length() > 0) {
			user.setLoginname(loginNameInput.getText());
		} else {
			error = true;
		}
		
		if(passwordInput.getText().length() > 0) {
			user.setPassword(passwordInput.getText());
		} else {
			error = true;
		}
		
		
		if(maritalStatusInput.getText().length() > 0) {
			user.setMarital_status(maritalStatusInput.getText());
		} else {
			error = true;
		}
		
		if(finalGradeInput.getText().length() > 0) {
			String grade = finalGradeInput.getText();
			user.setFinal_grade(Double.parseDouble(grade));
		} else {
			error = true;
		}
		
		
		if(error == false) {
			userController.createUser(user);
		
		} else {
			JOptionPane.showMessageDialog(null, "Nicht alle Felder wurden ausgefüllt.", "Fehler",
			JOptionPane.ERROR_MESSAGE);
		}
		
		
	}

}
