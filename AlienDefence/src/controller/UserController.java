package controller;

import model.User;
import model.persistance.IUserPersistance;
import model.persistanceDB.PersistanceDB;

/**
 * @class UserController
 * @version 1.0.0
 * @author Justin Riedel
 */
public class UserController {

	private IUserPersistance userPersistance;
		
	/**
	 * Creates a new object of a UserController, which persists user objects.
	 * @param userPersistance, user persistance layer
	 */
	public UserController(IUserPersistance userPersistance) {
		this.userPersistance = userPersistance;
	}
	
	/**
	 * Creates a user based on the given User Object attributes
	 * @param User, User-Object 
	 * @return User object, returns 0 if user allready exist
	 */
	public int createUser(User user) {
		User existingUser = readUser(user.getLoginname());
		if(existingUser == null) {	
			int userId = new PersistanceDB().getUserPersistance().createUser(user);
			return userId;
	    }
		return 0;
	}
	
	/**
	 * Reads an user from  persistence layer and returns the user object.
	 * @param username, users unique login name
	 * @return User object, returns null if the user does not exist
	 */
	public User readUser(String username) {
		User user = new PersistanceDB().getUserPersistance().readUser(username);
		if (user != null) {
			return user;
		}
		return null; 
	}
	
	/**
	 * Updates a user based on the given User Object attributes
	 * @param User, User-Object 
	 */
	public void changeUser(User user) {
		new PersistanceDB().getUserPersistance().updateUser(user);
	}
	
	/**
	 * Deletes an user. 
	 * @param User, User-Object
	 */
	public void deleteUser(User user) {
		new PersistanceDB().getUserPersistance().deleteUser(user);
	}
	
	/**
	 * Reads an user and checks if the given password matches with the stored password 
	 * @param username, users unique login name
	 * @param password, correct password string
	 * @return User object, returns null if the user does not exist
	 */
	public boolean checkPassword(String username, String passwort) {
		User user = readUser(username);
		if (user.getPassword().equals(passwort) && user != null) {
			return true;
		}
		return false;
	}
	
}
